﻿using w5.Extensions;
using w5.Logger;
using w5.Model;

namespace w5
{
    public class NumberOperation
    {
        private ILogger _logger;

        private List<Veshicle> _veshicle;

        Func<Veshicle, float> getValue = (person) => { return person.Price; };

        public NumberOperation(ILogger logger)
        {
            _logger = logger;
            _veshicle = new(){
                new Veshicle { Price = 51122 },
                new Veshicle { Price = 22222 },
                new Veshicle { Price = 35431 },
                new Veshicle { Price = 10000 },
            };
        }

        public void WriteResult()
        {
            _logger.Write("max Price = " + _veshicle.GetMax(getValue).Price);
        }
    }
}