﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace w5.Extensions
{
    public static class Extension
    {
        public static T GetMax<T>(this IEnumerable<T> veshicles, Func<T, float> getParameter) where T : class
        {
            var max = veshicles.First();
            foreach (var v in veshicles)
            {
                if (getParameter(max) < getParameter(v))
                {
                    max = v;
                }
            }
            return max;
        }
    }
}
