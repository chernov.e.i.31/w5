﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using w5.Logger;

namespace w5
{
    public class FileFounder
    {
        private ILogger _logger;
        private string _path;
        private bool _stop;
        public event FileSearchHandler FileHandler;
        public event ButtonEscHandler EscHandler;
        CancellationTokenSource _cancelTokenSource = new CancellationTokenSource();

        public FileFounder(ILogger logger, string path)
        {
            _logger = logger;
            _path = path;
        }

        public void Start()
        {
            FileHandler += ConsoleMessage;
            EscHandler += EscButtonTask;
            var _token = _cancelTokenSource.Token;

            ConsoleKey button = Console.ReadKey().Key;

            Task.Run(() =>
            {
                var files = Directory.GetFiles(_path, "*", SearchOption.AllDirectories);
                EscHandler(button);
                foreach (var f in files)
                {
                    Thread.Sleep(500);
                    FileHandler?.Invoke(f);
                    if (_token.IsCancellationRequested)
                    {
                        // Clean up here, then...
                        break;
                    }
                }
            }, _cancelTokenSource.Token).Wait();

            FileHandler -= ConsoleMessage;
            EscHandler -= EscButtonTask;
        }

        public void Start2()
        {
            FileHandler += ConsoleMessage;
            EscHandler += EscButtonButton;
            _stop = false;

            var files = Directory.GetFiles(_path, "*", SearchOption.AllDirectories);
            foreach (var f in files)
            {
                Thread.Sleep(500);
                FileHandler?.Invoke(f);
                if (Console.KeyAvailable)
                {
                    EscHandler(Console.ReadKey(true).Key);
                    if (_stop)
                        break;
                }
            }
            FileHandler -= ConsoleMessage;
            EscHandler -= EscButtonButton;
        }

        private void EscButtonTask(ConsoleKey btn)
        {
            if (btn == ConsoleKey.Spacebar)
                _cancelTokenSource.Cancel();
        }

        private void EscButtonButton(ConsoleKey btn)
        {
            if (btn == ConsoleKey.Spacebar)
                _stop = true;
        }
        private void ConsoleMessage(string message) => _logger.Write(message);
    }
    public delegate void FileSearchHandler(string path);
    public delegate void ButtonEscHandler(ConsoleKey btn);
}
